import random
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.exceptions import ConnectionException
from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusSerialClient as ModbusClient


class ModbusConnector():
    def __init__(self):
        self.client = 0
        self.statistics = {'MessagesReceived': 0,
                           'MessagesSent': 0}
        self._is_test_mode_enable = 0
        super().__init__()

    def configure_modbus(self, method, port, baudrate, timeout, parity, stopbit, bytesize):
        self.client = ModbusClient(method=method, port=port, baudrate=baudrate, timeout=timeout, parity=parity,
                                   stopbits=stopbit, bytesize=bytesize)
        if self.client.connect():
            return 1
        else:
            return 0

    def enable_test_mode(self):
        self._is_test_mode_enable = 1

    def modbus_read(self, slave_id, function_code, register_addr, data_type, register_count):
        read_value = 0
        if self._is_test_mode_enable:
            read_value = random.randint(1, 1000)
            return read_value
        else:
            try:
                if function_code == 4:
                    payload = self.client.read_input_registers(register_addr, register_count, unit=slave_id)
                elif function_code == 3:
                    payload = self.client.read_holding_registers(register_addr, register_count, unit=slave_id)
                elif function_code == 0:
                    payload = self.client.read_coils(register_addr, register_count, unit=slave_id)
                elif function_code == 1:
                    payload = self.client.read_discrete_inputs(register_addr, register_count, unit=slave_id)
                else:
                    return Exception
                
                if data_type == '32bit_float':
                    decoder = BinaryPayloadDecoder.fromRegisters(payload.registers, Endian.Big)
                    read_value = decoder.decode_32bit_float()
                elif data_type == 'bool':
                    read_value = payload.bits[0]
                    if read_value == True:
                        read_value = 1
                    elif read_value == False:
                        read_value = 0
                    else:
                        read_value = -999
                    
                    return read_value
                else:
                    decoder = BinaryPayloadDecoder.fromRegisters(payload.registers, Endian.Big)
                    read_value = decoder.decode_16bit_int()

                return read_value
            except:
                return -9999

    # coil write function
    def modbus_coil_write(self, slave_id, coil_addr, value):
        try:
            response = self.client.write_coil(address=coil_addr, value=value, unit=slave_id)
            return 1
        except:
            return 0
