#!/bin/bash

NOW=$(date +"%d-%m-%Y-%T")

gridx(){
if ps ax | grep -v grep | grep main.py > /dev/null
then
echo "Gridx script is runnning"
else

cd /home/gridx/qs_evaporator
echo "Starting Gridx script @ $NOW"
nohup python3 main.py &
fi
}

main(){
    gridx
}

main
