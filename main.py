from modbus_connector import ModbusConnector
from cloudx_client import Cloudx
import time
import threading
import json

method = 'rtu'
port = "/dev/ttyUSB0"
baudrate = 19200
timeout = 1
parity = 'N'
stopbit = 2
bytesize = 8

slave_id_plc_1 = 1
slave_id_plc_2 = 2


fan_coil = 0
pump_2_coil = 1
pump_coil = 2
flush_pump_coil = 3

auto_mode = 1

injection_1_time = 10
injection_2_time = 10
cycle_time = 30
flush_time = 120

read_data = {}
fan_on_cmd = 0
pump_on_cmd = 0
pump_2_on_cmd = 0
flush_pump_on_cmd = 0

modbus = 0
cloudx = 0
is_injection_1_over = False

def read_parameter(modbus, slave_id, function_code, register_addr, data_type, register_count,factor, offset):
    received_data = modbus.modbus_read(slave_id, function_code, register_addr, data_type, register_count)
    if function_code == 0 or function_code == 1:
        return received_data
    converted_data = round((float(received_data)/factor) + offset, 2)
    return converted_data

# fan_thread
def fan_thread():
    time.sleep(2)
    global modbus
    global fan_coil
    global read_data
    global auto_mode
    global fan_on_cmd
    global slave_id_plc_1

    while True:
        fan_status = read_data['values']['fan_status']
        if auto_mode:
            if not fan_status:
                modbus.modbus_coil_write(slave_id_plc_1, fan_coil, True)
        
        else:
            if fan_on_cmd and not fan_status:
                modbus.modbus_coil_write(slave_id_plc_1, fan_coil, True)
            elif not fan_on_cmd and fan_status:
                modbus.modbus_coil_write(slave_id_plc_1, fan_coil, False)

        time.sleep(1)

# injection_pump
def injection_thread():
    time.sleep(2)
    global modbus
    global pump_coil
    global injection_1_time
    global cycle_time
    global auto_mode
    global pump_on_cmd
    global read_data
    global is_injection_1_over
    global slave_id_plc_1
    busy_in_injection = False
    injection_start_time = 0

    while True:
        if auto_mode:
            modbus.modbus_coil_write(slave_id_plc_1, pump_coil, True)
            time.sleep(injection_1_time)
            is_injection_1_over = True
            modbus.modbus_coil_write(slave_id_plc_1, pump_coil, False)
            time.sleep(cycle_time - injection_1_time)
        
        else:
            pump_status = read_data['values']['pump_status']

            if pump_on_cmd and not busy_in_injection:
                modbus.modbus_coil_write(slave_id_plc_1, pump_coil, True)
                injection_start_time = time.time()
                busy_in_injection = True
            
            injection_current_time = time.time()
            injection_elapsed_time = injection_current_time - injection_start_time

            if injection_elapsed_time > injection_1_time and pump_status:
                modbus.modbus_coil_write(slave_id_plc_1, pump_coil, False)
                busy_in_injection = False
                pump_on_cmd = 0

            if not pump_on_cmd and pump_status:
                modbus.modbus_coil_write(slave_id_plc_1, pump_coil, False)
                busy_in_injection = False

# injection 2 thread for pump2
def injection_2_thread():
    time.sleep(2)
    global modbus
    global pump_2_coil
    global injection_1_time
    global injection_2_time
    global cycle_time
    global auto_mode
    global pump_2_on_cmd
    global read_data
    global is_injection_1_over
    global slave_id_plc_1
    busy_in_injection = False
    injection_start_time = 0

    while True:
        if auto_mode:
            if is_injection_1_over:
                modbus.modbus_coil_write(slave_id_plc_1, pump_2_coil, True)
                time.sleep(injection_2_time)
                modbus.modbus_coil_write(slave_id_plc_1, pump_2_coil, False)
                time.sleep(cycle_time - injection_2_time - injection_1_time)
                is_injection_1_over = False

            time.sleep(0.1)       
        else:
            pump_2_status = read_data['values']['pump_2_status']

            if pump_2_on_cmd and not busy_in_injection:
                modbus.modbus_coil_write(slave_id_plc_1, pump_2_coil, True)
                injection_start_time = time.time()
                busy_in_injection = True
            
            injection_current_time = time.time()
            injection_elapsed_time = injection_current_time - injection_start_time

            if injection_elapsed_time > injection_2_time and pump_2_status:
                modbus.modbus_coil_write(slave_id_plc_1, pump_2_coil, False)
                busy_in_injection = False
                pump_2_on_cmd = 0

            if not pump_2_on_cmd and pump_2_status:
                modbus.modbus_coil_write(slave_id_plc_1, pump_2_coil, False)
                busy_in_injection = False




# flush pump
def flush_thread():
    time.sleep(2)
    global modbus
    global flush_pump_coil
    global auto_mode
    global flush_time
    global flush_pump_on_cmd
    global read_data
    global slave_id_plc_1
    busy_in_flushing = False
    flush_start_time = 0

    while True:
        if auto_mode:
            float_switch_status = read_data['values']['float_switch']
            if float_switch_status:
                modbus.modbus_coil_write(slave_id_plc_1, flush_pump_coil, True)
                time.sleep(flush_time)
                modbus.modbus_coil_write(slave_id_plc_1, flush_pump_coil, False)
                # 
                # time.sleep(flush_time)

            else:
                time.sleep(0.5)

        else:
            flush_pump_status = read_data['values']['flush_pump_status']
            
            if flush_pump_on_cmd and not busy_in_flushing:
                modbus.modbus_coil_write(slave_id_plc_1, flush_pump_coil, True)
                flush_start_time = time.time()
                busy_in_flushing = True
            
            flush_current_time = time.time()
            flush_elapsed_time = flush_current_time - flush_start_time

            if flush_elapsed_time > flush_time and flush_pump_status:
                modbus.modbus_coil_write(slave_id_plc_1, flush_pump_coil, False)
                busy_in_flushing = False
                flush_pump_on_cmd = 0

            if not flush_pump_on_cmd and flush_pump_status:
                modbus.modbus_coil_write(slave_id_plc_1, flush_pump_coil, False)
                busy_in_flushing = False
# read modbus plc
def read_plc_thread():
    global read_data
    global modbus
    global cloudx
    global slave_id_plc_1
    global slave_id_plc_2
    payload = {}

    while True:
        # make address global
        value = read_parameter(modbus, slave_id_plc_1, 4, 4, '32bit_int', 1, 20, 0)
        payload['temp_in'] = value

        value = read_parameter(modbus, slave_id_plc_1, 4, 6, '32bit_int', 1, 20, 0)
        payload['temp_out'] = value

        value = read_parameter(modbus, slave_id_plc_1, 4, 12, '32bit_int', 1, 0.33, 18.18)
        payload['humidity_in'] = value

        value = read_parameter(modbus, slave_id_plc_1, 4, 14, '32bit_int', 1, 0.33, 18.18)
        payload['humidity_out'] = value

        value = read_parameter(modbus, slave_id_plc_1, 0, 0, 'bool', 1, 10, 0)
        payload['fan_status'] = value

        value = read_parameter(modbus, slave_id_plc_1, 0, 2, 'bool', 1, 10, 0)
        payload['pump_status'] = value

        value = read_parameter(modbus, slave_id_plc_1, 0, 1, 'bool', 1, 10, 0)
        payload['pump_2_status'] = value

        value = read_parameter(modbus, slave_id_plc_1, 0, 3, 'bool', 1, 10, 0)
        payload['flush_pump_status'] = value

        value = read_parameter(modbus, slave_id_plc_1, 1, 0, 'bool', 1, 10, 0)
        payload['float_switch'] = value

        value = read_parameter(modbus, slave_id_plc_2, 4, 4, '32bit_int', 1, 20, 0)
        payload['temp_3'] = value

        timestamp = int(time.time() * 1000)
        read_data['ts'] = timestamp
        read_data['values'] = payload

        #print(read_data)
        cloudx.send_telemetry_data(read_data)
        # if status:
        #     print("successfully sent: %s" % send_data)

        time.sleep(0.5)


def rpc_thread():
    global cloudx
    global modbus
    global auto_mode
    global injection_1_time
    global injection_2_time
    global cycle_time
    global flush_time
    global fan_on_cmd
    global pump_on_cmd
    global pump_2_on_cmd
    global flush_pump_on_cmd

    while True:
        rpc_cmd = cloudx.get_rpc_utility_json_data()
        print("RPC: ", rpc_cmd)
        if rpc_cmd:
            if rpc_cmd['method'] == 'auto_mode':
                auto_mode = int(rpc_cmd['params'])
                status = cloudx.send_attribute('auto_mode', auto_mode)
                if status:
                    print("auto mode updated: %d" % auto_mode)

            elif rpc_cmd['method'] == 'injection_time':         # to be mapped to injection_1_time
                try:
                    injection_1_time = int(rpc_cmd['params'])
                    status = cloudx.send_attribute('injection_1_time', injection_1_time)
                    if status:
                        print("injection_1 time updated: %d secs" % injection_1_time)
                except:
                    print("injection_1 time should be integer")
            
            elif rpc_cmd['method'] == 'injection_2_time':
                try:
                    injection_2_time = int(rpc_cmd['params'])
                    status = cloudx.send_attribute('injection_2_time', injection_2_time)
                    if status:
                        print("injection_2 time updated: %d secs" % injection_2_time)

                except:
                    print("injection_2 time should be integer")

            elif rpc_cmd['method'] == 'cycle_time':
                try:
                    cycle_time = int(rpc_cmd['params'])
                    status = cloudx.send_attribute('cycle_time', cycle_time)
                    if status:
                        print("cycle time updated: %d secs" % cycle_time)

                except:
                    print("cycle time should be integer")

            elif rpc_cmd['method'] == 'flush_time':
                try:
                    flush_time = int(rpc_cmd['params'])
                    status = cloudx.send_attribute('flush_time', flush_time)
                    if status:
                        print("flush time updated: %d secs" % flush_time)
                except:
                    print("flush_time should be integer")

            
            elif rpc_cmd['method'] == 'coil_0':
                if rpc_cmd['params']:
                    fan_on_cmd = 1
                else:
                    fan_on_cmd = 0
            
            elif rpc_cmd['method'] == 'coil_2':
                if rpc_cmd['params']:
                    pump_on_cmd = 1
                else:
                    pump_on_cmd = 0
            
            elif rpc_cmd['method'] == 'coil_1':
                if rpc_cmd['params']:
                    pump_2_on_cmd = 1
                else:
                    pump_2_on_cmd = 0

            elif rpc_cmd['method'] == 'coil_3':
                if rpc_cmd['params']:
                    flush_pump_on_cmd = 1
                else:
                    flush_pump_on_cmd = 0

            else:
                a = 1
        
        time.sleep(1)

def attr_thread():
    global cloudx
    global auto_mode
    global injection_1_time
    global injection_2_time
    global flush_time
    global cycle_time

    while True:
        status = cloudx.send_attribute('auto_mode', auto_mode)
        if status:
            print("auto mode updated: %d" % auto_mode)
        status = cloudx.send_attribute('injection_1_time', injection_1_time)
        if status:
            print("injection_1 time updated: %d secs" % injection_1_time)
        status = cloudx.send_attribute('injection_2_time', injection_2_time)
        if status:
            print("injection_2 time updated: %d secs" % injection_2_time)
        status = cloudx.send_attribute('cycle_time', cycle_time)
        if status:
            print("cycle time updated: %d secs" % cycle_time)
        status = cloudx.send_attribute('flush_time', flush_time)
        if status:
            print("flush time updated: %d secs" % flush_time)

        print("Attributes updated.")
        time.sleep(30)


def main():
    global modbus
    global cloudx
    modbus = ModbusConnector()
    cloudx = Cloudx()

    status = modbus.configure_modbus(method, port, baudrate, timeout, parity, stopbit, bytesize)
    print(status)
    if status:
        t1 = threading.Thread(target=fan_thread)
        t2 = threading.Thread(target=injection_thread)
        t7 = threading.Thread(target=injection_2_thread)
        t3 = threading.Thread(target=flush_thread)
        t4 = threading.Thread(target=read_plc_thread)
        t5 = threading.Thread(target=rpc_thread)
        t6 = threading.Thread(target=attr_thread)

        t1.start()
        t3.start()
        t2.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()


main()
