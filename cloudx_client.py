import requests
from requests.exceptions import HTTPError
import time
import json
import threading

access_token = "UNQGRD-" + "GXQSC00001"

class Cloudx:
    rpc_json_id = 0

    def __init__(self):
        global access_token
        self.data = []
        server_address = "http://portal.uniqgridcloud.com"
        server_port = "8080"
        self.cloudx_url = server_address + ':' + server_port
        self.access_token = access_token

    def send_telemetry_data(self, data):
        send_data = json.dumps(data)
        workerTh = threading.Thread(target=self.send_telemetry, args=(send_data,))
        workerTh.start()

    def send_telemetry(self, data):
        headers = {
            'Content-Type': 'application/json'
        }

        # self.log_msg.debug("Server URL: %s and access token: %s" % (self.cloudx_url, self.access_token))
        # self.log_msg.debug("--------------sending telemetry data (id: %d)-----------" % self.demon_id)
        # self.log_msg.debug("Telemetry data: %s" % data)

        url = self.cloudx_url + '/api/v1/' + self.access_token + '/telemetry'

        _is_send_success = False

        try:
            response = requests.post(url, headers=headers, data=data)
            # self.log_msg.debug("Response (id: %d): %s" % (self.demon_id, response))
            if response.status_code == 200:
                # self.log_msg.info("Send telemetry data success (id: %d)" % self.demon_id)
                _is_send_success = True 
            else:
                _is_send_success = False
                

        except ConnectionRefusedError:
            # self.log_msg.warning("Conn Refused, Server not running or not accepting more connections (id: %d)"
            #                      % self.demon_id)
            _is_send_success = False
        except TimeoutError:
            # self.log_msg.warning("Timeout occur (id: %d)" % self.demon_id)
            _is_send_success = False
        except requests.exceptions.RequestException:
            # self.log_msg.exception("Unprecedented Exception occurred in module %s" % __file__, exc_info=True)
            # self.log_msg.error("Unble to sent data to the server (id: %d)" % self.demon_id)
            _is_send_success = False

        return _is_send_success
        
    def send_attribute(self, attribute_key, attribute_data):
        data = '{"' + attribute_key + '": "' + str(attribute_data) + '"}'
        headers = {
            'Content-Type': 'application/json'
        }

        # self.log_msg.debug("Server URL: %s and access token: %s" % (self.cloudx_url, self.access_token))
        # self.log_msg.debug("--------------sending attribute data (id: %d)-----------" % self.demon_id)
        # self.log_msg.debug(data)

        url = self.cloudx_url + '/api/v1/' + self.access_token + '/attributes'

        try:
            response = requests.post(url, headers=headers, data=data)
            # self.log_msg.debug("Response (id: %d): %s" % (self.demon_id, response))
            if response.status_code == 200:
                # self.log_msg.info("Send attribute data success (id: %d)" % self.demon_id)
                return True

        except ConnectionRefusedError:
            # self.log_msg.warning(
            #     "Conn Refused, Server not running or not accepting more connections (id: %d)" % self.demon_id)
            return 0
        except requests.exceptions.RequestException:
            # self.log_msg.exception("Unprecedented Exception occurred in module %s" % __file__, exc_info=True)
            # self.log_msg.error("Not able to sent data to the server (id: %d)" % self.demon_id)
            return 0

    
    def get_rpc_utility_json_data(self):
        global access_token

        url = self.cloudx_url + '/api/v1/' + access_token + '/rpc'

        try:
            response = requests.get(url)
            # self.log_msg.debug("RPC received response: %s" % response.text)
            # print(response.text)
            json_data = json.loads(response.text)
        except:
            # self.log_msg.error("Could not connect to RPC")
            # time.sleep(system_flags.rpc_utility_timeout)
            return 0

        # sends this when timeout or no input d=from rpc
        # {"timestamp":1594482327841,"status":503,"error":"Service Unavailable",
        # "exception":"org.springframework.web.context.request.async.AsyncRequestTimeoutException",
        # "message":"No message available","path":"/api/v1/UNQGRD-GX000001/rpc"} 

        if not "method" in json_data:
            # self.log_msg.debug("Timeout: data not received from RPC")
            return 0

        if not json_data['id'] > Cloudx.rpc_json_id:
            return 0

        Cloudx.rpc_json_id = json_data['id']
        # self.send_rpc_reply(json_data['id'])

        return json_data 

    def send_rpc_reply(self, id):
        global access_token
        headers = {
            'Content-Type': 'application/json'
        }

        reply_url = self.cloudx_url + '/api/v1/' + access_token + '/rpc/' + str(id)

        try:
            data = '{"result" : "success" }'
            response = requests.post(reply_url, headers=headers, data=data)
            # self.log_msg.debug("RPC sent reply response: %s" % response.text)
        except:
            # self.log_msg.error("Could not connect to RPC, unable to send reply")
            a = 1


